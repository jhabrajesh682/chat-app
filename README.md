# Realtime Chat App

The Installation process assumes that you already have the above technologies installed on your machine.

Navigate to the folder and run:

```
npm install
```

To start the server, you can run:

```
npm start
```

The server should start at port 8090 (default). Navigate to [http://localhost:8090](http://localhost:8090) to see the demo.

Although, before doing that, you might want to flush the DB and start with 0 users.

For that, you can uncomment the line in `index.js` that says `client.flushdb();`

Once that is done, you are ready to go. Remember to restart the server if you intend to make any changes to the code.
