const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const jwt = require('jsonwebtoken');
const jwtPrivatKey = process.env.JWT_PRIVATE_KEY

console.log(jwtPrivatKey);
const userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        maxlength: 50,
        minlength: 3,
        required: true
    },

    password: {
        type: String,
        required: true
    },

}, {
    timestamps: true
});



userSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({ _id: this._id }, jwtPrivatKey);
    return token;
}

module.exports = Users = mongoose.model("Users", userSchema);
