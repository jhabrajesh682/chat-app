
const { Room } = require('colyseus')

class ChatRoom extends Room {
    maxClients = 4;


    onCreate(options) {

        console.log("ChatRoom created! by brajeshh", options);


        this.onMessage("message", (client, message) => {
            console.log("ChatRoom received message from", client.sessionId, ":", message);
            this.broadcast("messages", `(${client.sessionId}) ${message}`);
        });
    }

    onJoin(client) {
        // console.log("client====>", client);

        this.broadcast("messages", `${client.sessionId} joined.`);
    }


    onLeave(client) {
        this.broadcast("messages", `${client.sessionId} left.`);
    }
    getAvailableRooms(client) {
        this.broadcast("messages").then(rooms => {
            rooms.forEach((room) => {
                console.log(room.roomId);
                console.log(room.clients);
                console.log(room.maxClients);
                console.log(room.metadata);
            })
        })
    }

    onDispose() {
        console.log("Dispose ChatRoom");
    }


    //     client.getAvailableRooms("battle").then(rooms => {
    //         rooms.forEach((room) => {
    //             console.log(room.roomId);
    //             console.log(room.clients);
    //             console.log(room.maxClients);
    //             console.log(room.metadata);
    //         });
    //       }).catch (e => {
    //     console.error(e);
    // });
}
// export class ChatRoom extends Room {
//     // this room supports only 4 clients connected


// }

module.exports = ChatRoom
