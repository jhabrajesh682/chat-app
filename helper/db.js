const mongoose = require("mongoose");
require("dotenv").config();
const mongodbConnectionString = process.env.MONGODB_CONNECTION_STRING;


module.exports = function () {
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    mongoose.set('useUnifiedTopology', true);
    mongoose.connect(mongodbConnectionString)

    mongoose.connection.on("connected", function () {
        console.log("Mongoose default connection is open ");
    });

    mongoose.connection.on("error", function (err) {
        console.log
            ("Mongoose default connection has occured " + err + " error")
            ;
    });

    mongoose.connection.on("disconnected", function () {
        console.log("Mongoose default connection is disconnected")
    });
}