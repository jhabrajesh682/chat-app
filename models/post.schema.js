const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const postSchema = new Schema({
    postTitle: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },

    tags: [{
        type: String,
        required: true
    }],
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    },

}, {
    timestamps: true
});




module.exports = Users = mongoose.model("post", postSchema);
