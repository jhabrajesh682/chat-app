$(function () {

    // test
    let socket = io();
    let chatter_count;
    $.get('/get_chatters', function (response) {
        $('.chat-info').text("There are currently " + response.length + " people in the chat room");
        chatter_count = response.length; //update chatter count
    });
    $('#join-chat').click(function () {
        let username = $.trim($('#username').val());
        let password = $.trim($('#pass').val());
        $.ajax({
            url: '/join',
            type: 'POST',
            data: {
                username: username,
                password: password
            },
            success: function (response) {
                console.log("response=====>", response);
                if (response.status == 'OK') {
                    window.location.href = "/01-chat.html";
                    // socket.emit('message', {
                    //     'username': username,
                    //     'message': username + " has Joined the chat room.."
                    // });
                    // socket.emit('update_chatter_count', {
                    //     'action': 'increase'
                    // });
                    $('.chat').show();
                    $('#leave-chat').data('username', username);
                    $('#send-message').data('username', username);
                    $.get('/get_messages', function (response) {
                        if (response.length > 0) {

                            let message_count = response.length;
                            let html = '';
                            for (let x = 0; x < message_count; x++) {
                                html += "<div class='msg'><div class='user'>" + response[x]['sender'] + "</div><div class='txt'>" + response[x]['message'] + "</div></div>";
                            }
                            $('.messages').html(html);
                        }
                    });
                    $('.join-chat').hide();
                } else if (response.status == 'FAILED') {
                    alert("Sorry but the username already exists, please choose another one");
                    $('#username').val('').focus();
                }
                else if (response.status == 'false') {
                    alert(response.message);
                    $('#username').val('').focus();
                    $('#pass').val('').focus();
                }
            },
            Error: function name(response) {
                console.log(response);
            }
        });
    });
    $('#leave-chat').click(function () {
        let username = $(this).data('username');
        $.ajax({
            url: '/leave',
            type: 'POST',
            dataType: 'json',
            data: {
                username: username
            },
            success: function (response) {
                if (response.status == 'OK') {
                    socket.emit('message', {
                        'username': username,
                        'message': username + " has left the chat room.."
                    });
                    socket.emit('update_chatter_count', {
                        'action': 'decrease'
                    });
                    $('.chat').hide();
                    $('.join-chat').show();
                    $('#username').val('');
                    alert('You have successfully left the chat room');
                }
            }
        });
    });
    $('#send-message').click(function () {
        let username = $(this).data('username');
        let message = $.trim($('#message').val());
        $.ajax({
            url: '/send_message',
            type: 'POST',
            dataType: 'json',
            data: {
                'username': username,
                'message': message
            },
            success: function (response) {
                if (response.status == 'OK') {
                    socket.emit('message', {
                        'username': username,
                        'message': message
                    });
                    $('#message').val('');
                }
            }
        });
    });

    $('#register-user').click(function () {
        let username = $.trim($('#username').val());
        let password = $.trim($('#pass').val());
        let email = $.trim($('#addEmail').val());
        console.log("username=====>", username);
        console.log("password=====>", password);
        console.log("email=====>", email);
        $.ajax({
            url: '/register',
            type: 'POST',
            dataType: 'json',
            data: {
                'email': email,
                'username': username,
                'password': password
            },
            success: function (response) {

                if (response.status == true) {
                    alert(response.message)
                }
            },
            error: function name(response) {

                alert('please try again with different username or Email Id')
            }

        });
    });
    // socket.on('send', function (data) {
    //     let username = data.username;
    //     let message = data.message;
    //     let html = "<div class='msg'><div class='user'>" + username + "</div><div class='txt'>" + message + "</div></div>";
    //     $('.messages').append(html);
    // });
    // socket.on('count_chatters', function (data) {
    //     if (data.action == 'increase') {
    //         chatter_count++;
    //     } else {
    //         chatter_count--;
    //     }
    //     $('.chat-info').text("There are currently " + chatter_count + " people in the chat room");
    // });
});

