const express = require('express');
const dotenv = require('dotenv');
dotenv.config({ path: './config/config.env' });
const bodyParser = require('body-parser');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const db = require('./helper/db')
const fs = require('fs');
let creds = '';
const cors = require('cors');
const ChatRoom = require('./rooms/01-chat-room');
const user = require('./models/user.schema')
let port = process.env.PORT || 8080;
const redis = require('redis');
let client = '';
const serveIndex = require('serve-index');
const { Server } = require('colyseus')
const { monitor } = require('@colyseus/monitor')
const path = require('path');


const gameServer = new Server({
    server: http,
    express: app,
    pingInterval: 0,
});

gameServer.define("chat", ChatRoom)
    .enableRealtimeListing();

gameServer.define("chat_with_options", ChatRoom, {
    custom_options: "you can use me on Room#onCreate"
});

app.use(express.urlencoded());

// Parse JSON bodies (as sent by API clients)
app.use(express.json());
// (optional) attach web monitoring panel
app.use('/colyseus', monitor());

gameServer.onShutdown(function () {
    console.log(`game server is going down.`);
});



client = redis.createClient();

// Redis Client Ready
client.once('ready', function () {

    // Flush Redis DB
    client.flushdb();

    // Initialize Chatters
    client.get('chat_users', function (err, reply) {
        if (reply) {
            chatters = JSON.parse(reply);
        }
    });

    // Initialize Messages
    client.get('chat_app_messages', function (err, reply) {
        if (reply) {
            chat_messages = JSON.parse(reply);
        }
    });
});
// });

gameServer.listen(port);
db();
user();
console.log('Server Started and Listening on :' + port);
// Start the Server
// http.listen(port, );

// Store people in chatroom
let chatters = [];

// Store messages in chatroom
let chat_messages = [];

// Express Middleware
app.use(express.static('public'));
// app.use(bodyParser.urlencoded({
//     extended: true
// }));

let allowedOrigins = ['http://localhost:4200',
    'http://localhost:8090'];
app.use(cors({
    origin: function (origin, callback) {
        // allow requests with no origin 
        // (like mobile apps or curl requests)
        if (!origin) return callback(null, true);
        if (allowedOrigins.indexOf(origin) === -1) {
            var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    }
}));

//app.use('/', serveIndex(path.join(__dirname, "static"), { 'icons': true }))
app.use('/', express.static(path.join(__dirname, "static")));
// Render Main HTML file
app.get('/', function (req, res) {
    res.sendFile('views/index.html', {
        root: __dirname
    });
});

// Render Register HTML file
app.get('/registerpage', function (req, res) {
    res.sendFile('views/register.html', {
        root: __dirname
    });
});


// API -  Register new user
app.post('/register', async (req, res) => {
    console.log("body====>", req.body);
    let email = req.body.email
    try {
        let existingUsers = await user.findOne({ email: email, username: req.body.username }).lean();

        if (existingUsers) {
            return res.status(400).send({
                status: false,
                message: "user already exist"
            })
        }

        let users = new user({
            ...req.body
        })
        await users.save();
        return res.status(200).send({
            status: true,
            message: "users successfully created"
        })
    } catch (error) {
        console.log(error);
    }

})

app.use("/api/v1/post", require("./routes/post.routes"));
// API - Login user and generate auth token

app.post('/login', async (req, res) => {

    let checkUser = await user.findOne({ username: req.body.username, password: req.body.password }).lean();

    if (!checkUser) {
        return res.status(404).send({
            status: false,
            message: "user not found"
        })
    }
    let email = checkUser.email
    console.log("email====>", email);
    const token = checkUser.generateAuthToken()


    return res.status(200).send({
        message: "successfully logged in",
        token: token
    })
})

// API - Join Chat
app.post('/join', async (req, res) => {
    let userResponse
    let username = req.body.username;
    let password = req.body.password
    console.log(username, password);
    client.get(username, async (err, user) => {
        if (user) {
            userResponse = JSON.parse(user)
            console.log("userResponse====>", userResponse);
            if (userResponse.password === password && userResponse.username === username) {
                console.log(`${username} existed in cache DB`);
                return res.send({
                    'status': 'OK',
                    'result': userResponse
                })
            }
            else {
                return res.send({
                    status: false,
                    message: "password or username incorrect"
                })
            }
        }

    })

    let checkUser = await user.findOne({ username: req.body.username, password: req.body.password }).lean();
    console.log("check====>", checkUser);

    if (!checkUser) {
        return res.send({
            'status': 'false',
            'message': "user not found your username or password is incorrect"
        })
    }
    client.setex(username, 1440, JSON.stringify(checkUser));
    res.send({
        'status': 'OK',
        'result': checkUser
    })
    // }

    // })

    // client.get(username, async (err, user) => {
    //     console.log("user=====>", user);
    //     if (user) {
    //         return res.send({
    //             'status': 'OK',
    //             'message': 'user from cache'
    //         })
    //     }

    // })

    // if (chatters.indexOf(username) === -1) {
    //     chatters.push(username);
    //     client.set('chat_users', JSON.stringify(chatters));
    //     res.send({
    //         'chatters': chatters,
    //         'status': 'OK',
    //         // 'token': token
    //     });
    // } else {
    //     res.send({
    //         'status': 'FAILED'
    //     });
    // }
});

// API - Leave Chat
app.post('/leave', function (req, res) {
    let username = req.body.username;
    chatters.splice(chatters.indexOf(username), 1);
    client.set('chat_users', JSON.stringify(chatters));
    res.send({
        'status': 'OK'
    });
});

// API - Send + Store Message
app.post('/send_message', function (req, res) {
    let username = req.body.username;
    let message = req.body.message;
    chat_messages.push({
        'sender': username,
        'message': message
    });
    client.set('chat_app_messages', JSON.stringify(chat_messages));
    res.send({
        'status': 'OK'
    });
});

// API - Get Messages
app.get('/get_messages', function (req, res) {
    res.send(chat_messages);
});

// API - Get Chatters
app.get('/get_chatters', function (req, res) {
    console.log("chatters=====>", chatters);
    res.send(chatters);
});


// io.on('connection', function (socket) {

//     socket.on('message', function (data) {
//         io.emit('send', data);
//     });

//     socket.on('update_chatter_count', function (data) {
//         io.emit('count_chatters', data);
//     });

// });
