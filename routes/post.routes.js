const router = require("express").Router();
const posts = require("../collection/post.collection")



const post = new posts();
/**
 * @type Express.Router
 *
 * @api - /api/v1/users/create @method - POST
 */
router.get("/", post.getAllPost);
router.post("/create", post.createPost);


router.put("/:id", post.getOnePostAndUpdate)

router.delete("/:id", post.getonePostAndRemove)




module.exports = router;